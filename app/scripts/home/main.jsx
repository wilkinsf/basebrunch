define([], function () {
  "use strict";

  var ExampleComponent = React.createClass({displayName: 'ExampleComponent',

    render:function(){
      return(
        <div>Loading with RequireJS</div>
      );
    }

  });

  return ExampleComponent;
});
