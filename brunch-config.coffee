exports.config =
  # See http://brunch.io/#documentation for docs.
  files:
    javascripts:
      joinTo:
          'scripts/app.js': /^app/
          'scripts/vendor.js': /^vendor/
      order:
        before: [
          '/vendor/scripts/require',
          '/vendor/scripts/react-with-addons-0.11.2.js'

        ]
    stylesheets:
      joinTo: 'styles/app.css'
      order:
        before: []
